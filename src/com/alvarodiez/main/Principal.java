package com.alvarodiez.main;

import com.alvarodiez.gui.Controlador;
import com.alvarodiez.gui.Modelo;
import com.alvarodiez.gui.Vista;

public class Principal {
    public static void main(String[] args) {
        Vista ventana = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(modelo, ventana);
    }

}

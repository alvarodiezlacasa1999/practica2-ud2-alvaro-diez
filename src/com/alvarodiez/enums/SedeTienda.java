package com.alvarodiez.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox sedeTienda de la vista.
 * Representan las sedes de las tiendas que existen.
 */
public enum SedeTienda {
    MUNICH("Munich, Alemania"),
    FRANKFURT("Frankfurt, Alemania"),
    PARIS("Paris, Francia"),
    MADRID("Madrid, España"),
    BARCELONA("Barcelona, España"),
    ROMA("Roma, Italia"),
    TURIN("Turin, Italia"),
    LONDRES("Londres, Reino Unido"),
    ATENAS("Atenas, Grecia"),
    OPORTO("Oporto, Portugal");

    private String valor;

    SedeTienda(String valor) { this.valor=valor; }
    public String getValor() {
        return valor;
    }
}

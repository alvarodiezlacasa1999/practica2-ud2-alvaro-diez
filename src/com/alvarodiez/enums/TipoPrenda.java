package com.alvarodiez.enums;

/**
 * Esta clase enum enumera las constantes con las que se rellena
 * el JComoboBox tipoPrenda de la vista.
 * Representan las sedes de las tiendas que existen.
 */
public enum TipoPrenda {
    PANTALON("Pantalón"),
    CAMISETA("Camiseta"),
    SUDADERA("Sudadera"),
    ABRIGO("Abrigo"),
    CHAQUETA("Chaqueta"),
    CAMISA("Camisa"),
    POLO("Polo"),
    BLUSA("Blusa"),
    FALDA("Falda"),
    ROPAINTERIOR("Ropa Interior"),
    CALCETINES("Calcetines"),
    ZAPATILLAS("Zapatillas"),
    GORRA("Gorra"),
    GORRO("Gorro");

    private String valor;

    TipoPrenda(String valor) { this.valor=valor; }
    public String getValor() {
        return valor;
    }
}

package com.alvarodiez.gui;

import com.alvarodiez.util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    /**
     * Constructor de la clase, inicializa el modelo y la vista, carga los datos
     * desde un fichero, añade los listeners y lista los datos.
     *
     * @param modelo Modelo
     * @param vista  Vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.vista = vista;
        this.modelo = modelo;

        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();

    }

    /**
     * Método para refrescar todas las tablas a la vez
     */
    private void refrescarTodo() {
        refrescarTienda();
        refrescarFabrica();
        refrescarRopa();
        refrescar = false;
    }

    /**
     * Añade ActionListeners a los botones y menus de la vista
     *
     * @param listener ActionListener que se añade
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnNuevaRopa.addActionListener(listener);
        vista.btnNuevo2.addActionListener(listener);
        vista.btnNuevo3.addActionListener(listener);
        vista.btnEliminar1.addActionListener(listener);
        vista.btnEliminar2.addActionListener(listener);
        vista.btnEliminar3.addActionListener(listener);
        vista.btnModif1.addActionListener(listener);
        vista.btnModif2.addActionListener(listener);
        vista.btnModif3.addActionListener(listener);
        vista.optionDialog.btnGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidar.addActionListener(listener);
    }

    private void addItemListeners(Controlador controlador) { }

    /**
     * Añade WindowListeners a la vista
     *
     * @param listener WindowListener que se añade
     */
    private void addWindowListeners(WindowListener listener) { vista.addWindowListener(listener); }

    /**
     * Actualiza las fábricas que se ven en la lista y los comboboxes
     */
    private void refrescarFabrica() {
        try {
            vista.tablaFabrica.setModel(constrDTMFabrica(modelo.consultarFabrica()));
            vista.cbFabricaRopa.removeAllItems();
            for(int i = 0; i < vista.dtmFabrica.getRowCount(); i++) {
                vista.cbFabricaRopa.addItem(vista.dtmFabrica.getValueAt(i, 0)+" - "+
                        vista.dtmFabrica.getValueAt(i, 2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método para construir la tabla de fábricas
     *
     * @param rs
     * @return tabla de fábricas
     * @throws SQLException
     */
    private DefaultTableModel constrDTMFabrica(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmFabrica.setDataVector(data, columnNames);

        return vista.dtmFabrica;
    }

    /**
     * Actualiza las tiendas que se ven en la lista y los comboboxes
     */
    private void refrescarTienda() {
        try {
            vista.tablaTienda.setModel(constrDTMTienda(modelo.consultarTienda()));
            vista.cbTiendaRopa.removeAllItems();
            for(int i = 0; i < vista.dtmTienda.getRowCount(); i++) {
                vista.cbTiendaRopa.addItem(vista.dtmTienda.getValueAt(i, 0)
                        +" - "+ vista.dtmTienda.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método para construir la tabla de tiendas
     *
     * @param rs
     * @return tabla de tiendas
     * @throws SQLException
     */
    private DefaultTableModel constrDTMTienda(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmTienda.setDataVector(data, columnNames);

        return vista.dtmTienda;
    }

    /**
     * Actualiza las prendas que se ven en la lista y los comboboxes
     */
    private void refrescarRopa() {
        try {
            vista.tablaRopa.setModel(constrDTMRopa(modelo.consultarRopa()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Método para construir la tabla de ropa
     *
     * @param rs
     * @return tabla de ropas
     * @throws SQLException
     */
    private DefaultTableModel constrDTMRopa(ResultSet rs) throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmRopa.setDataVector(data, columnNames);

        return vista.dtmRopa;
    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Setea el dialog de opciones según las opciones que el usuario guardo en la
     * última ejecución del programa.
     */
    private void setOptions() {
        vista.optionDialog.tIP.setText(modelo.getIP());
        vista.optionDialog.tUsuario.setText(modelo.getUser());
        vista.optionDialog.tPassword.setText(modelo.getPassword());
        vista.optionDialog.tAdminPassword.setText(modelo.getAdminPassword());
    }

    /**
     * Vacía los campos de la tabla de ropas
     */
    private void borrarCamposRopa() {
        vista.cbTipoRopa.setSelectedIndex(-1);
        vista.tCodigoRopa.setText("");
        vista.cbFabricaRopa.setSelectedIndex(-1);
        vista.cbTiendaRopa.setSelectedIndex(-1);
        vista.tMarcaRopa.setText("");
        vista.tMaterialRopa.setText("");
        vista.tTallaRopa.setText("");
        vista.tPrecioRopa.setText("");
    }

    /**
     * Vacía los campos de la tabla de tiendas
     */
    private void borrarCamposTienda() {
        vista.tNombreTienda.setText("");
        vista.tWebTienda.setText("");
        vista.tTlfnTienda.setText("");
        vista.tEmailTienda.setText("");
        vista.cbSedeTienda.setSelectedIndex(-1);
    }

    /**
     * Vacía los campos de la tabla de fábricas
     */
    private void borrarCamposFabrica() {
        vista.tTipoFabrica.setText("");
        vista.tNombreFabrica.setText("");
        vista.tPaisFabrica.setText("");
        vista.dpFechaFact.setText("");
    }

    /**
     * Comprueba que los campos necesarios para añadir una prenda estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarRopaVacia() {
        return  vista.cbTipoRopa.getSelectedIndex() == -1 ||
                vista.tCodigoRopa.getText().isEmpty() ||
                vista.cbFabricaRopa.getSelectedIndex() == -1 ||
                vista.cbTiendaRopa.getSelectedIndex() == -1 ||
                vista.tMarcaRopa.getText().isEmpty() ||
                vista.tMaterialRopa.getText().isEmpty() ||
                vista.tTallaRopa.getText().isEmpty() ||
                vista.tPrecioRopa.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir una tienda estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarTiendaVacia() {
        return  vista.tNombreTienda.getText().isEmpty() ||
                vista.tWebTienda.getText().isEmpty() ||
                vista.tTlfnTienda.getText().isEmpty() ||
                vista.tEmailTienda.getText().isEmpty() ||
                vista.cbSedeTienda.getSelectedIndex() == -1;
    }

    /**
     * Comprueba que los campos para añadir una fábrica estén vacios
     *
     * @return true si como mínimo está vacio un campo
     */
    private boolean comprobarFabricaVacia() {
        return  vista.tTipoFabrica.getText().isEmpty() ||
                vista.tNombreFabrica.getText().isEmpty() ||
                vista.tPaisFabrica.getText().isEmpty() ||
                vista.dpFechaFact.getText().isEmpty();
    }

    /**
     * Añade acciones cuando la ventana se cierra. Si el autoguardado está activado la ventana se cerrará
     * sin más, volcando todos los datos en el fichero. De lo contrario pueden ocurrir dos cosas:
     * Si todos los cambios han sido guardados, la ventana se cerrará.
     *
     * @param e Evento producido al tratar de cerrar la ventana
     */
    @Override
    public void windowClosing(WindowEvent e) {
        Util.showWarningAlert("Se va cerrar la ventana");
        System.exit(0);
    }

    /**
     * Añade sus respectivas acciones a los botones de añadir, modificar y eliminar de las listas.
     * Añade sus respectivas acciones a los menus de guardar, guardar como y mostrar opciones.
     * Añade la acción de guardar las opciones al dialog de opciones.
     * Añade las acciones de guardar del dialog.
     *
     * @param e Evento producido al pulsar un botón o menu
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;

            case "Desconectar":
                modelo.desconectar();
                break;

            case "Salir":
                JOptionPane close = new JOptionPane("Se va a cerrar la ventana",
                        JOptionPane.WARNING_MESSAGE, JOptionPane.OK_OPTION, null);
                System.exit(0);
                break;
            case "abrir Opciones":
                if(String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "guardar Opciones":
                modelo.setPropValues(vista.optionDialog.tIP.getText(), vista.optionDialog.tUsuario.getText(),
                        String.valueOf(vista.optionDialog.tPassword.getPassword()), String.valueOf(vista.optionDialog.tAdminPassword.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;
            case "nuevaRopa": {
                try {
                    if (comprobarRopaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaRopa.clearSelection();
                    } else if (modelo.codigoYaExiste(vista.tCodigoRopa.getText())) {
                        Util.showErrorAlert("Ese código ya existe.\nIntroduce una prenda diferente");
                        vista.tablaRopa.clearSelection();
                    } else {
                        modelo.insertarRopa(
                                String.valueOf(vista.cbTipoRopa.getSelectedItem()),
                                vista.tCodigoRopa.getText(),
                                String.valueOf(vista.cbFabricaRopa.getSelectedItem()),
                                String.valueOf(vista.cbTiendaRopa.getSelectedItem()),
                                vista.tMarcaRopa.getText(),
                                vista.tMaterialRopa.getText(),
                                vista.tTallaRopa.getText(),
                                Float.parseFloat(vista.tPrecioRopa.getText()));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaRopa.clearSelection();
                }
                borrarCamposRopa();
                refrescarRopa();
            }
            break;

            case "modificarRopa":
                try {
                    if (comprobarRopaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaRopa.clearSelection();
                    } else {
                        modelo.modificarRopa(
                                String.valueOf(vista.cbTipoRopa.getSelectedItem()),
                                vista.tCodigoRopa.getText(),
                                String.valueOf(vista.cbFabricaRopa.getSelectedItem()),
                                String.valueOf(vista.cbTiendaRopa.getSelectedItem()),
                                vista.tMarcaRopa.getText(),
                                vista.tMaterialRopa.getText(),
                                vista.tTallaRopa.getText(),
                                Float.parseFloat(vista.tPrecioRopa.getText()),
                                Integer.parseInt((String)vista.tablaRopa.getValueAt(vista.tablaRopa.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaRopa.clearSelection();
                }
                borrarCamposRopa();
                refrescarRopa();
                break;

            case "eliminarRopa":
                modelo.borrarRopa(Integer.parseInt((String)vista.tablaRopa.getValueAt(vista.tablaRopa.getSelectedRow(), 0)));
                borrarCamposRopa();
                refrescarRopa();
                break;

            case "nuevaTienda":
                try {
                    if (comprobarTiendaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaTienda.clearSelection();
                    } else if (modelo.tiendaYaExiste(vista.tNombreTienda.getText())) {
                        Util.showErrorAlert("Esa tienda ya existe.\nIntroduce una tienda diferente");
                        vista.tablaTienda.clearSelection();
                    } else {
                        modelo.insertarTienda(vista.tNombreTienda.getText(),
                                vista.tWebTienda.getText(),
                                Float.parseFloat(vista.tTlfnTienda.getText()),
                                vista.tEmailTienda.getText(),
                                String.valueOf(vista.cbSedeTienda.getSelectedItem()));

                        refrescarTienda();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaTienda.clearSelection();
                }
                borrarCamposTienda();
                break;

            case "modificarTienda":
                try {
                    if (comprobarTiendaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaTienda.clearSelection();
                    } else {
                        modelo.modificarTienda(
                                vista.tNombreTienda.getText(),
                                vista.tWebTienda.getText(),
                                Float.parseFloat(vista.tTlfnTienda.getText()),
                                vista.tEmailTienda.getText(),
                                String.valueOf(vista.cbSedeTienda.getSelectedItem()),
                                Integer.parseInt((String)vista.tablaTienda.getValueAt(vista.tablaTienda.getSelectedRow(), 0)));
                        refrescarTienda();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaTienda.clearSelection();
                }
                borrarCamposTienda();
                break;

            case "eliminarTienda":
                modelo.borrarTienda(Integer.parseInt((String)vista.tablaTienda.getValueAt(vista.tablaTienda.getSelectedRow(), 0)));
                borrarCamposTienda();
                refrescarTienda();
                break;

            case "nuevaFabrica":
                try {
                    if (comprobarFabricaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaFabrica.clearSelection();
                    } else if (modelo.fabricaYaExiste(vista.tNombreFabrica.getText())) {
                        Util.showErrorAlert("Esa fábrica ya existe.\nIntroduce una fábrica diferente.");
                        vista.tablaFabrica.clearSelection();
                    } else {
                        modelo.insertarFabrica(vista.tTipoFabrica.getText(),
                                vista.tNombreFabrica.getText(),
                                vista.tPaisFabrica.getText(),
                                vista.dpFechaFact.getDate());
                        refrescarFabrica();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaFabrica.clearSelection();
                }
                borrarCamposFabrica();
                break;

            case "modificarFabrica":
                try {
                    if (comprobarFabricaVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.tablaFabrica.clearSelection();
                    } else {
                        modelo.modificarFabrica(vista.tTipoFabrica.getText(),
                                vista.tNombreFabrica.getText(),
                                vista.tPaisFabrica.getText(),
                                vista.dpFechaFact.getDate(),
                                Integer.parseInt((String)vista.tablaFabrica.getValueAt(vista.tablaFabrica.getSelectedRow(), 0)));
                        refrescarFabrica();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.tablaFabrica.clearSelection();
                }
                borrarCamposFabrica();
                break;

            case "eliminarEditorial":
                modelo.borrarFabrica(Integer.parseInt((String)vista.tablaFabrica.getValueAt(vista.tablaFabrica.getSelectedRow(), 0)));
                borrarCamposFabrica();
                refrescarFabrica();
                break;

        }
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     * @param e Evento producido en una lista
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.tablaFabrica.getSelectionModel())) {
                int row = vista.tablaFabrica.getSelectedRow();
                vista.tTipoFabrica.setText(String.valueOf(vista.tablaFabrica.getValueAt(row, 1)));
                vista.tNombreFabrica.setText(String.valueOf(vista.tablaFabrica.getValueAt(row, 2)));
                vista.tPaisFabrica.setText(String.valueOf(vista.tablaFabrica.getValueAt(row, 3)));
                vista.dpFechaFact.setDate((Date.valueOf(String.valueOf(vista.tablaFabrica.getValueAt(row, 4)))).toLocalDate());
            } else if (e.getSource().equals(vista.tablaTienda.getSelectionModel())) {
                int row = vista.tablaTienda.getSelectedRow();
                vista.tNombreTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row, 1)));
                vista.tWebTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row, 2)));
                vista.tTlfnTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row, 3)));
                vista.tEmailTienda.setText(String.valueOf(vista.tablaTienda.getValueAt(row, 4)));
                vista.cbSedeTienda.setSelectedItem(String.valueOf(vista.tablaTienda.getValueAt(row, 5)));
            } else if (e.getSource().equals(vista.tablaRopa.getSelectionModel())) {
                int row = vista.tablaRopa.getSelectedRow();
                vista.cbTipoRopa.setSelectedItem(String.valueOf(vista.tablaRopa.getValueAt(row, 1)));
                vista.tCodigoRopa.setText(String.valueOf(vista.tablaRopa.getValueAt(row, 2)));
                vista.cbFabricaRopa.setSelectedItem(String.valueOf(vista.tablaRopa.getValueAt(row, 3)));
                vista.cbTiendaRopa.setSelectedItem(String.valueOf(vista.tablaRopa.getValueAt(row, 4)));
                vista.tMarcaRopa.setText(String.valueOf(vista.tablaRopa.getValueAt(row, 5)));
                vista.tMaterialRopa.setText(String.valueOf(vista.tablaRopa.getValueAt(row, 6)));
                vista.tTallaRopa.setText(String.valueOf(vista.tablaRopa.getValueAt(row, 7)));
                vista.tPrecioRopa.setText(String.valueOf(vista.tablaRopa.getValueAt(row, 8)));
            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.tablaFabrica.getSelectionModel())) {
                    borrarCamposFabrica();
                } else if (e.getSource().equals(vista.tablaTienda.getSelectionModel())) {
                    borrarCamposTienda();
                } else if (e.getSource().equals(vista.tablaRopa.getSelectionModel())) {
                    borrarCamposRopa();
                }
            }
        }
    }

    //Métodos sin usar

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}

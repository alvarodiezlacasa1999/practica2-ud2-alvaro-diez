package com.alvarodiez.gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

public class Modelo {

    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Constructor de la clase Modelo.
     *
     * Interpreta el archivo config.properties y establece los atributos que hayamos indicado
     */
    public Modelo() {
        getPropValues();
    }

    String getIP() { return ip; }
    String getUser() { return user; }
    String getPassword() { return password; }
    String getAdminPassword() { return adminPassword; }


    /**
     * Método para conectar el programa con la base de datos pertinente
     */
    private Connection conexion;
    public void conectar() {
        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/baseropa",user, password);
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://"+ip+":3306/",user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Método para desconectar la base del programa
     */
    public void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    /**
     * Método para insertar un nuevo registro en la tabla ropas.
     *
     * @param tipo_prenda
     * @param codigo
     * @param fabrica
     * @param tienda
     * @param marca
     * @param material
     * @param talla
     * @param precio
     */
    public void insertarRopa(String tipo_prenda, String codigo, String fabrica, String tienda,  String marca,
                             String material, String talla, float precio) {
        String consulta = "INSERT INTO ropas (tipo_prenda, codigo, idfabrica, idtienda, marca, material, talla, precio) VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement sentencia = null;

        int idfabrica = Integer.valueOf(fabrica.split(" ")[0]);
        int idtienda = Integer.valueOf(tienda.split(" ")[0]);

        try {
            sentencia=conexion.prepareStatement(consulta);
            sentencia.setString(1, tipo_prenda);
            sentencia.setString(2, codigo);
            sentencia.setInt(3, idfabrica);
            sentencia.setInt(4, idtienda);
            sentencia.setString(5, marca);
            sentencia.setString(6, material);
            sentencia.setString(7, talla);
            sentencia.setDouble(8, precio);
            sentencia.executeUpdate();
        } catch (SQLException e){
            System.out.println(e);
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para insertar un nuevo registro en la tabla tiendas.
     *
     * @param tienda
     * @param email
     * @param telefono
     * @param web
     * @param sede
     */
    public void insertarTienda(String tienda, String email, float telefono,
                             String web, String sede) {
        String consulta = "INSERT INTO tiendas (tienda, email, telefono, web, sede) VALUES (?,?,?,?,?)";
        PreparedStatement sentencia = null;

        try {
            sentencia=conexion.prepareStatement(consulta);
            sentencia.setString(1, tienda);
            sentencia.setString(2, email);
            sentencia.setFloat(3, telefono);
            sentencia.setString(4, web);
            sentencia.setString(5, sede);
            sentencia.executeUpdate();
        } catch (SQLException e){
            System.out.println(e);
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para insertar un nuevo registro en la tabla fabricas.
     *
     * @param fabrica
     * @param tipo_fabrica
     * @param pais
     * @param fecha_facturacion
     */
    public void insertarFabrica(String fabrica, String tipo_fabrica,
                               String pais, LocalDate fecha_facturacion) {
        String consulta = "INSERT INTO fabricas (fabrica, tipo_fabrica, pais, fecha_facturacion) VALUES (?,?,?,?)";
        PreparedStatement sentencia = null;

        try {
            sentencia=conexion.prepareStatement(consulta);
            sentencia.setString(1, fabrica);
            sentencia.setString(2, tipo_fabrica);
            sentencia.setString(3, pais);
            sentencia.setDate(4, Date.valueOf(fecha_facturacion));
            sentencia.executeUpdate();
        } catch (SQLException e){
            System.out.println(e);
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para modificar todos los campos de la tabla ropas.
     *
     * @param tipo_prenda
     * @param codigo
     * @param id_fabrica
     * @param id_tienda
     * @param marca
     * @param material
     * @param talla
     * @param precio
     * @param idprenda
     */
    public void modificarRopa(String tipo_prenda, String codigo, String id_fabrica, String id_tienda, String marca,
                            String material, String talla, float precio, int idprenda){

        String consulta = "UPDATE ropas SET tipo_prenda = ?, codigo = ?,  idfabrica = ?, idtienda = ?, marca = ?," +
                " material = ?, talla = ?, precio = ? WHERE idprenda = ?";
        PreparedStatement sentencia = null;

        int idfabrica = Integer.parseInt(id_fabrica.split(" ")[0]);
        int idtienda = Integer.parseInt(id_tienda.split(" ")[0]);

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, tipo_prenda);
            sentencia.setString(2, codigo);
            sentencia.setInt(3, idfabrica);
            sentencia.setInt(4, idtienda);
            sentencia.setString(5, marca);
            sentencia.setString(6, material);
            sentencia.setString(7, talla);
            sentencia.setDouble(8, precio);
            sentencia.setInt(9, idprenda);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para modificar todos los campos de la tabla tiendas.
     *
     * @param tienda
     * @param email
     * @param telefono
     * @param web
     * @param sede
     * @param idtienda
     */
    public void modificarTienda(String tienda, String email, float telefono,
                                String web, String sede, int idtienda){

        String consulta = "UPDATE tiendas SET tienda = ?, email = ?, telefono = ?," +
                " web = ?, sede = ? WHERE idtienda = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, tienda);
            sentencia.setString(2, email);
            sentencia.setFloat(3, telefono);
            sentencia.setString(4, web);
            sentencia.setString(5, sede);
            sentencia.setInt(6, idtienda);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para modificar todos los campos de la tabla fabricas.
     *
     * @param fabrica
     * @param tipo_fabrica
     * @param pais
     * @param fecha_facturacion
     * @param idfabrica
     */
    public void modificarFabrica(String fabrica, String tipo_fabrica,
                                 String pais, LocalDate fecha_facturacion, int idfabrica){

        String consulta = "UPDATE fabricas SET fabrica = ?, tipo_fabrica = ?, pais = ?," +
                " fecha_facturacion = ? WHERE idfabrica = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setString(1, fabrica);
            sentencia.setString(2, tipo_fabrica);
            sentencia.setString(3, pais);
            sentencia.setDate(4, Date.valueOf(fecha_facturacion));
            sentencia.setInt(5, idfabrica);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para borrar las prendas
     *
     * @param idprenda
     */
    public void borrarRopa(int idprenda) {
        String consulta = "DELETE FROM ropas WHERE idprenda = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idprenda);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para borrar las tiendas
     *
     * @param idtienda
     */
    public void borrarTienda(int idtienda) {
        String consulta = "DELETE FROM tiendas WHERE idtienda = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idtienda);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Método para borrar las fábricas
     *
     * @param idfabrica
     */
    public void borrarFabrica(int idfabrica) {
        String consulta = "DELETE FROM fabricas WHERE idfabrica = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(consulta);
            sentencia.setInt(1, idfabrica);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
        }
    }

    /**
     * Este metodo devuelve las tiendas guardadas en la base de datos
     *
     * @return consulta
     * @throws SQLException
     */
    public ResultSet consultarTienda() throws SQLException {
        String sentenciaSql = "SELECT concat(idtienda) as 'ID', concat(tienda) as 'Nombre tienda', concat(web) as 'Web'," +
                "concat(telefono) as 'Teléfono', concat(email) as 'Email', concat(sede) as 'Sede' FROM tiendas";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Este metodo devuelve las fábricas guardadas en la base de datos
     *
     * @return consulta
     * @throws SQLException
     */
    public ResultSet consultarFabrica() throws SQLException {
        String sentenciaSql = "SELECT concat(idfabrica) as 'ID', concat(fabrica) as 'Nombre fábrica', concat(tipo_fabrica) as 'Tipo fábrica', " +
                "concat(pais) as 'Pais', concat(fecha_facturacion) as 'Fecha facturación' FROM fabricas";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Este metodo devuelve las prendas guardadas en la base de datos
     *
     * @return consulta
     * @throws SQLException
     */
    ResultSet consultarRopa() throws SQLException {
        String sentenciaSql = "SELECT concat(ropa.idprenda) as 'ID', concat(ropa.tipo_prenda) as 'Tipo', concat(ropa.codigo) as 'Código', " +
                "concat(fabrica.idfabrica, ' - ', fabrica.fabrica) as 'Fábrica'," +
                "concat(tienda.idtienda, ' - ', tienda.tienda) as 'Tienda', " +
                "concat(ropa.marca) as 'Marca', concat(ropa.material) as 'Material', concat(ropa.talla) as 'Talla'," +
                "concat(ropa.precio) as 'Precio' FROM ropas as ropa " +
                "inner join fabricas as fabrica on fabrica.idfabrica = ropa.idfabrica inner join " +
                "tiendas as tienda on tienda.idtienda = ropa.idtienda";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Lee el fichero .sql donde se encuentra el código para crear de la base de datos
     *
     * @return código archivo sql
     * @throws IOException
     */
    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("baseDatosVariasTablas_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    /**
     * Actualiza los parámetros del archivo config.properties
     *
     * @param ip dirección ip de la base de datos
     * @param user nombre de usuario en la base de datos
     * @param pass contraseña de usuario en la base de datos
     * @param adminPass contraseña del admin
     */
    public void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    /**
     * Interpreta el archivo config.properties y establece los atributos que hayamos indicado
     */
    private void getPropValues() {
        InputStream entrada = null;
        try {
            Properties props = new Properties();
            String propFileName = "config.properties";

            entrada=new FileInputStream(propFileName);

            props.load(entrada);
            ip = props.getProperty("ip");
            user = props.getProperty("user");
            password = props.getProperty("pass");
            adminPassword = props.getProperty("admin");

        } catch (IOException e) {
            System.out.println(e);
        } finally {
            try {
                if (entrada != null) entrada.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Comprueba si existe una fábrica comparando el nombre con todas
     * las fábricas de la base
     *
     * @param fabrica nombre de la fabrica
     * @return true si ya existe
     */
    public boolean fabricaYaExiste(String fabrica) {
        String consultaFabrica = "SELECT existeFabrica(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(consultaFabrica);
            function.setString(1, fabrica);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    /**
     * Comprueba si existe una tienda comparando el nombre con todas
     * las tiendas de la base
     *
     * @param tienda nombre de la tienda
     * @return true si ya existe
     */
    public boolean tiendaYaExiste(String tienda) {
        String consultaTienda = "SELECT existeTienda(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(consultaTienda);
            function.setString(1, tienda);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    /**
     * Comprueba si existe una prenda de ropa comparando el código con todas
     * las prendas de la base
     *
     * @param codigo código del libro
     * @return true si ya existe
     */
    public boolean codigoYaExiste(String codigo) {
        String consultaRopa = "SELECT existeCodigo(?)";
        PreparedStatement function;
        boolean isbnExists = false;
        try {
            function = conexion.prepareStatement(consultaRopa);
            function.setString(1, codigo);
            ResultSet rs = function.executeQuery();
            rs.next();

            isbnExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isbnExists;
    }


}

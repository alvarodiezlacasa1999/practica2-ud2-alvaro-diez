package com.alvarodiez.gui;

import javax.swing.*;
import java.awt.*;

public class OptionDialog extends JDialog{
    private JPanel panel1;
    JButton btnGuardar;
    JTextField tIP;
    JTextField tUsuario;
    JPasswordField tAdminPassword;
    JPasswordField tPassword;
    private Frame admin;

    /**
     * Constructor de la clase OptionDialog
     *
     * @param admin propietario del JDialog
     */
    public OptionDialog(Frame admin) {
        super(admin, "Opciones", true);
        this.admin = admin;
        startDialog();
    }

    /**
     * Inicia el JDialog
     */
    private void startDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()));
        this.setLocationRelativeTo(admin);


    }


}

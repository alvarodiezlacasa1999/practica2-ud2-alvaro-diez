package com.alvarodiez.gui;

import com.alvarodiez.enums.SedeTienda;
import com.alvarodiez.enums.TipoPrenda;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JPanel Ropa;
    private JPanel Tienda;
    private JPanel Fabrica;

    //Ropa
    JTextField tCodigoRopa;
    JTextField tMarcaRopa;
    JTextField tMaterialRopa;
    JTextField tTallaRopa;
    JTextField tPrecioRopa;
    JComboBox cbFabricaRopa;
    JComboBox cbTiendaRopa;
    JComboBox cbTipoRopa;
    JButton btnNuevaRopa;
    JButton btnModif1;
    JButton btnEliminar1;
    JTable tablaRopa;
    //Tienda
    JTextField tNombreTienda;
    JTextField tWebTienda;
    JTextField tTlfnTienda;
    JTextField tEmailTienda;
    JComboBox cbSedeTienda;
    JButton btnNuevo2;
    JButton btnEliminar2;
    JButton btnModif2;
    JTable tablaTienda;
    //Fabrica
    JTextField tTipoFabrica;
    JTextField tPaisFabrica;
    JTextField tNombreFabrica;
    DatePicker dpFechaFact;
    JButton btnNuevo3;
    JButton btnEliminar3;
    JButton btnModif3;
    JTable tablaFabrica;
    //DTM's
    DefaultTableModel dtmFabrica;
    DefaultTableModel dtmTienda;
    DefaultTableModel dtmRopa;
    //MENUBAR
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    //OptionDialog
    OptionDialog optionDialog;
    //SAVECHANGESDIALOG
    JDialog adminPasswordDialog;
    JButton btnValidar;
    JPasswordField adminPassword;

    JLabel lblAccion;

    /**
     * Constructor de la clase Vista.
     * Establece el nombre que aparece en el marco de la ventana y la inicializa
     */
    public Vista() {
        startJFrame();
    }

    /**
     * Configura el JFrame y lo muestra
     */
    private void startJFrame() {
        setTitle("Alvaro Diez Lacasa");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+75, this.getHeight()+50));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);
        setDTM();
        setAdmin();
        setMenu();
        setEnumComboBox();
    }

    /**
     * Establece el contenido de los combobox con los datos de las enumeraciones
     */
    private void setEnumComboBox() {
        for(TipoPrenda constant : TipoPrenda.values()) { cbTipoRopa.addItem(constant.getValor()); }
        cbTipoRopa.setSelectedIndex(-1);

        for(SedeTienda constant : SedeTienda.values()) { cbSedeTienda.addItem(constant.getValor()); }
        cbSedeTienda.setSelectedIndex(-1);
    }

    /**
     * Inicializa los table models y los establece en sus respectivas tablas
     */
    private void setDTM() {
        this.dtmRopa = new DefaultTableModel();
        this.tablaRopa.setModel(dtmRopa);

        this.dtmTienda = new DefaultTableModel();
        this.tablaTienda.setModel(dtmTienda);

        this.dtmFabrica = new DefaultTableModel();
        this.tablaFabrica.setModel(dtmFabrica);
    }

    /**
     * Crea una barra de menú
     */
    private void setMenu(){
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    /**
     *  Establece el JDialog para poder introducir la contraseña del admin y configurar la base
     */
    private void setAdmin() {
        btnValidar = new JButton("Validar");
        btnValidar.setActionCommand("abrir Opciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidar};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

}

CREATE DATABASE if not exists baseRopa;
--
USE baseRopa;
--

CREATE TABLE IF NOT EXISTS fabricas (
idfabrica int auto_increment primary key,
fabrica varchar(50) not null,
tipo_fabrica varchar(50) not null,
pais varchar(50),
fecha_facturacion date
);

CREATE TABLE IF NOT EXISTS tiendas (
idtienda int auto_increment primary key,
tienda varchar(50) not null,
email varchar(50),
telefono varchar(9),
web varchar(500),
sede varchar(100) not null
);


CREATE TABLE IF NOT EXISTS ropas (
idprenda int auto_increment primary key,
tipo_prenda varchar(50) not null,
codigo varchar(50) not null UNIQUE,
marca varchar(50) not null,
material varchar(50) not null,
talla varchar(20) not null,
precio double not null,
idfabrica int not null,
idtienda int not null
);
--
alter table ropas
add foreign key (idfabrica) references fabricas(idfabrica),
add foreign key (idtienda) references tiendas(idtienda);
--

create function existeCodigo(f_codigo varchar(40))
returns bit
READS SQL DATA
DETERMINISTIC
begin
	declare i int;
	set i=0;
	while (i<(select max(idprenda) from ropas)) do
	if ((select codigo from ropas 
		 where idprenda=(i+1)) like f_codigo) 
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
end;
--
create function existeFabrica (f_fabrica varchar(50))
returns bit
READS SQL DATA
DETERMINISTIC
begin
	declare i int;
	set i=0;
	while (i<(select max(idfabrica) from fabricas)) do
	if ((select fabrica from fabricas 
	     where idfabrica = (i+1)) like f_fabrica) 
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
end;
--
create function existeTienda (f_tienda varchar(50))
returns bit
READS SQL DATA
DETERMINISTIC
begin
	declare i int;
	set i=0;
	while (i<(select max(idtienda) from tiendas)) do
	if ((select concat(tienda,', ',sede) from tiendas
		where idtienda = (i+1)) like f_tienda)
	then return 1;
	end if;
	set i=i+1;
	end while;
	return 0;
end;


